<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Radio Shack University DBA</h1>
<div class="row">
	<div class="col-sm-9">
		<form is="orly-form" id="form">
			<div class="form-group">
				<label for="course">Course ID</label> 
				<select name="course.courseId">
					<c:if test="${empty courses}">
						<option value="1">Default</option>
					</c:if>
					<c:forEach items="${courses}" var="entry">
						<option value="${entry.courseId}">
							<c:out value="${entry.courseName}" /></option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="enrollDate">Enrollment Date</label> 
				<!--<input name="enrollDate">-->
					
				<input type="date" name="enrollDate" min="1970-01-01"/>
					
			</div>
			<div class="form-group">
				<label for="firstName">First Name</label> <input name="firstName"/>
			</div>
			<div class="form-group">
				<label for="lastName">Last Name</label> <input name="lastName"/>
			</div>
			<button type="btn" id="submitBtn" class="btn btn-primary">Submit</button>
		</form>

		<br />

		<orly-table id="studentTable" class="invisible" loaddataoncreate
			url='<c:url value="/university/getStudents" />' includefilter
			bordered maxrows="10" tabletitle="Search Results" class="invisible">
		<orly-column field="txId" label="Transaction ID" class=""
			sorttype="natural"></orly-column>
			
			
			
			
			
			<orly-column field="courseId" label="Course">
			
			<div slot="cell">
			
			\${model.course.courseName}
			
			
		
    	
  			</div>
			
			
			</orly-column>
			
			
			
			
			 <orly-column field="firstName"
			label="First Name"></orly-column> <orly-column field="lastName"
			label="Last Name"></orly-column> <orly-column field="enrollDate"
			label="Enroll Date"></orly-column> </orly-table>
	</div>
</div>

<script>
document.getElementById("submitBtn").addEventListener("click", function(e){
    try {
        e.preventDefault(); 

        let values = orly.qid("form").values;

        // Make AJAX call to the server along with a message
        fetch("<c:url value='/university/addStudent/add' />", {
                method: "POST",
                body: JSON.stringify(values),//JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                }
        }).then(function(response) {
        	 console.log(response)
               if (response.ok) {
                   console.log("response.status=", response.status);
                   
                   let jsonReceived = response.json();    

                   console.log(jsonReceived)
                   return jsonReceived
               }
         }).then(function(response) {
             orly.qid("studentTable").loadData();
             
             orly.qid('alerts').createAlert({'msg': response, 'type': "info", 'duration': 3000});

         }).catch(function(error) {
             console.log('There was a problem with your fetch operation');
         });

     } catch (err) {
         // Do not show try-catch errors to the user in production
         console.log('Error: ' + err)
     }
 });
	
</script>