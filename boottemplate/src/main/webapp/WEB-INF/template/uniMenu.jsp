<%@ include file="/WEB-INF/layouts/include.jsp"%>
<nav class="nav nav-pills flex-column mt-2">
    <a class="${active eq 'add' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
       href="<c:url value='/university/addStudent' />">New Student</a>
    <a class="${active eq 'H4666ST' ? 'nav-item nav-link active' : 'nav-item nav-link'}"
        href="<c:url value='/university/report'/>">Course Report</a> 
</nav>