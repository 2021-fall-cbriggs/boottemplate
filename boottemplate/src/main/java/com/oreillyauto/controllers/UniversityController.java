package com.oreillyauto.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.Student;
import com.oreillyauto.service.UniversityService;

@Controller
public class UniversityController {
    
    @Autowired
    UniversityService service;
    
    @GetMapping(value="/university/addStudent")
    public String addStudentPage(Model model) {
        model.addAttribute("courses",service.getCourses());
        return "addStudent";
    }
    
    @GetMapping(value="/university/report")
    public String reportPage(Model model) {
        model.addAttribute("courses",service.getCourses());
        return "report";
    }
    
    //requested using a fetch
    @ResponseBody
    @PostMapping(value="/university/addStudent/add")
    public String addStudentPost(Model model, @RequestBody Student student/*Integer courseId, String enrollDate, String firstName, String lastName*/) {
        
        try {
            
            
            
            /*Student newStudent = new Student();
            newStudent.setCourseId(101);//Integer.parseInt(allParams.get("courseId"))
            newStudent.setEnrollDate(allParams.get("enrollDate"));
            newStudent.setFirstName(allParams.get("firstName"));
            newStudent.setLastName(allParams.get("lastName"));*/
            
            //System.out.println("New student: "+newStudent.toString());
            service.addStudent(student);
            
            return "Added new transaction successfully";
            
        } catch(Exception e) {
            //e.printStackTrace();
            System.out.println("Error adding student to the database!");
            System.out.println("Error: "+e.getLocalizedMessage());
            //logOreilly(e);
            
            return "Failed to add transaction";
        }
        
        
    }
    
    public void logOreilly(Exception e) {
        betterOutput(e,"com.oreillyauto");
    }
    
    public void betterOutput(Exception e, String pkg) {
        StackTraceElement[] ste = e.getStackTrace();
        for (int i = 0; i < ste.length; i++) {
            String str = ste[i].toString();
            if (pkg == null) {
                System.out.println(str);
            } else {
                if (str.contains(pkg)) {
                    System.out.println(str);
                }
            }
        }
    }
    
    @ResponseBody
    @GetMapping(value="/university/getCourses")
    public List<Course> getCourses() {
        return service.getCourses();
    }
    
    @ResponseBody
    @GetMapping(value="/university/getStudents")
    public List<Student> getStudents() {
        return service.getStudents();
    }
}
