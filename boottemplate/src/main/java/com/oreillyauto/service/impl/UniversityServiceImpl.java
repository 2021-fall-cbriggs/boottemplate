package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CourseRepository;
import com.oreillyauto.dao.StudentRepository;
import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.Student;
import com.oreillyauto.service.UniversityService;

@Service("universityService")
public class UniversityServiceImpl implements UniversityService {
 
    @Autowired
    CourseRepository courseRepo;
    
    @Autowired
    StudentRepository studentRepo;
    
    @Override
    public List<Course> getCourses() {
        return courseRepo.getAllCourses();
    }

    @Override
    public boolean addStudent(Student s) {
        
        //studentRepo.addStudent(s); querydsl save unimplemented yet
        Student stu = studentRepo.save(s);
        return stu != null;
        
    }

    @Override
    public Course getCourseById(int id) {
        return courseRepo.getCourseById(id);
    }

    @Override
    public List<Student> getStudents() {
        return studentRepo.getAllStudents();
    }

}
