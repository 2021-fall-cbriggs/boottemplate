package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.Student;

public interface UniversityService {
    List<Course> getCourses();
    List<Student> getStudents();
    boolean addStudent(Student s);
    Course getCourseById(int id);
}
