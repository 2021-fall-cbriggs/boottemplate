package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.CourseRepositoryCustom;
import com.oreillyauto.domain.Course;

public interface CourseRepository extends CrudRepository<Course,Integer>, CourseRepositoryCustom {
    
}
