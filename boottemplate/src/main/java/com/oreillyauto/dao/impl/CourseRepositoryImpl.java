package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CourseRepositoryCustom;
import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.QCourse;

@Repository
public class CourseRepositoryImpl extends QuerydslRepositorySupport implements CourseRepositoryCustom {

    QCourse courseTable = QCourse.course;
    
    public CourseRepositoryImpl() {
        super(Course.class);
    }

    @Override
    public List<Course> getAllCourses() {
        System.out.println("The Courses: ");
        List<Course> c = from(courseTable).fetch();
        System.out.println("Courses: ");
        System.out.println(c);
        return c;
    }

    @Override
    public Course getCourseById(int id) {
        return from(courseTable)
                .where(courseTable.courseId.eq(id))
                .limit(1)
                .fetchOne();
    }

}
