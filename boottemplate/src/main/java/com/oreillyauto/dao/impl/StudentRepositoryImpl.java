package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.StudentRepositoryCustom;
import com.oreillyauto.domain.QStudent;
import com.oreillyauto.domain.Student;

@Repository
public class StudentRepositoryImpl extends QuerydslRepositorySupport implements StudentRepositoryCustom {

    QStudent studentTable = QStudent.student;
    
    public StudentRepositoryImpl() {
        super(Student.class);
    }

    @Override
    public boolean addStudent(Student s) {
        //FIX THIS
        return false;
    }

    @Override
    public List<Student> getAllStudents() {
        return from(studentTable).fetch();
    }

}
