package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Course;

public interface CourseRepositoryCustom {
    public List<Course> getAllCourses();
    public Course getCourseById(int id);
}
