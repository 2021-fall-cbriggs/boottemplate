package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Student;

public interface StudentRepositoryCustom {
    public boolean addStudent(Student s);
    public List<Student> getAllStudents();
}
