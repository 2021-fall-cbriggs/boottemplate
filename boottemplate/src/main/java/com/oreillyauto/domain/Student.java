package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="students")
public class Student implements Serializable{
    private static final long serialVersionUID = 4947711451649476563L;
    
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="tx_id", columnDefinition="INTEGER")
    private Integer txId;
    
    //many students to one remote course
    @ManyToOne
    //@Column(name="course_id", columnDefinition="INTEGER")
    @JoinColumn(name = "course_id", referencedColumnName = "course_id", columnDefinition = "INTEGER")
    private Course course;
    
    @Column(name="first_name", columnDefinition="VARCHAR(128)")
    private String firstName;
    
    @Column(name="last_name", columnDefinition="VARCHAR(128)")
    private String lastName;
    
    @Column(name="enroll_date", columnDefinition="TIMESTAMP")
    private String enrollDate;

    public Integer getTxId() {
        return txId;
    }

    public void setTxId(Integer txId) {
        this.txId = txId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    

    
}