package com.oreillyauto.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="COURSES")
public class Course implements Serializable {
    private static final long serialVersionUID = -6214558790190231487L;

    public Course() {
        super();
    }    

    public Course(Integer courseId, String courseName, String professor, String days, Integer duration) {
        super();
        this.courseId = courseId;
        this.courseName = courseName;
        this.professor = professor;
        this.days = days;
        this.duration = duration;
    }

    @Id
    @Column(name="course_id", columnDefinition="INTEGER")
    private Integer courseId;
    
    @Column(name="course_name", columnDefinition="VARCHAR(128)")
    private String courseName;
    
    @Column(name="professor", columnDefinition="VARCHAR(128)")
    private String professor;
    
    @Column(name="days", columnDefinition="VARCHAR(128)")
    private String days;
    
    @Column(name="duration", columnDefinition="INTEGER")
    private Integer duration;
    
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "course")
    private List<Student> students = new ArrayList<Student>();

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "Course [courseId=" + courseId + ", courseName=" + courseName + ", professor=" + professor + ", days=" + days + ", duration="
                + duration + "]";
    }
}