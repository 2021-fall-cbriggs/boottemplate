drop table if exists students;
drop table if exists courses;

create table students (
	tx_id INTEGER auto_increment,
    course_id integer,
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    enroll_date TIMESTAMP,
    primary key (tx_id)
);

create table courses (
    course_id integer,
    course_name VARCHAR(128),
    professor VARCHAR(128),
    days VARCHAR(128),
    duration integer,
    primary key (course_id)
);

insert into students(course_id, first_name, last_name, enroll_date) 
values (101, 'John', 'Smith', '2021-10-29');

insert into courses(course_id, course_name, professor, days, duration) 
values (101, 'Intro to CSC', 'Jones', 'M/W/F', '50'),
	   (201, 'Databases', 'Brannon', 'T/TH', '50');